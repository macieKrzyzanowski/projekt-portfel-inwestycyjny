<?php

$config = require_once 'config.php';

try 
{
	//Połączenie z baza 
	//1 - parametry polaczenia
	//2 - uzytkownik
	//3 - haslo
	//4 - atrybuty polaczenia
	$db = new PDO("mysql:host={$config['host']};dbname={$config['database']};charset=utf8", $config['user'], $config['password'], [
		PDO::ATTR_EMULATE_PREPARES => false, 
		PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
	]);
	
	
}
catch(PDOException $error)
{
	echo $error;
	exit('Database error');
}


?>