<?php
	session_start();
	
	if((isset($_POST['nick'])) && (isset($_POST['haslo'])))
	{
		$nick = filter_input(INPUT_POST, 'nick');
		$haslo = filter_input(INPUT_POST, 'haslo');
		
		require_once "database.php"; //polaczenie z baza danych

		//przygotowanie tresc zapytania
		$userQuery = $db->prepare('SELECT id_gracza, imie, nazwisko, nick, haslo FROM gracze WHERE nick = :nick');
		$userQuery->bindValue(':nick', $nick, PDO::PARAM_STR);
		$userQuery->execute();
		
		$tab_asoc = $userQuery->fetch(); //tworzenie tablicy asocjacyjnej ze zanlezionym recordem
									 //jezeli tablica jest pusta, to zwroci false 
		//udane logowanie
		if($tab_asoc && ($haslo == $tab_asoc['haslo']))
		{
			$_SESSION['id_gracza'] = $tab_asoc['id_gracza'];
			$_SESSION['imie'] = $tab_asoc['imie'];
			$_SESSION['nazwisko'] = $tab_asoc['nazwisko'];
			
			unset($_SESSION['bad_attempt']);
			header('Location: strona_glowna.php');
		}
		else
		{
			$_SESSION['bad_attempt'] = true;
			header('Location: index.php');
			exit();
		}
	}

?>

<!DOCTYPE HTML>

<html lang="pl">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Portfel inwestycyjny - </title>
	
	<link rel="stylesheet" href="main.css">
	<link rel="stylesheet" href="css/fontello.css">
    <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
    <![endif]-->
	
</head>
<body>
	
	<header>
		<h1 class="logo"><i class="demo-icon icon-money"></i>  Portfel inwestycyjny</h1>
		<nav> </nav>
		
	</header>
	
	<div class="container">
		
		<main>
			<article>
				<h1>Zaloguj się:</h1>
				<form method="post">
					<label>
						Login: <br/>  <input type="text" name="nick"/> <br/> 
						Hasło: <br/>  <input type="password" name="haslo"/> 
						<?php 
							if(isset($_SESSION['bad_attempt']))
							{
								echo "Błędny login lub hasło.";
							}
							unset($_SESSION['bad_attempt']);
						?> 
					</label>
					</br><input type="submit" value="Zaloguj sie">
					<br/><br/> 
					<a href="rejestracja.php"> Rejestracja - załóż darmowe konto! </a>
				</form>
			</article>
		</main>
	<div>
</body>
</html>