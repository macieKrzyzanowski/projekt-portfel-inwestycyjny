<?php
	session_start();
	
	if((isset($_POST['nick'])))
	{
		$ALL_OK = true; //falaga wykorzystywana do walidacji
		
		//Sprawdzenie nicku
		$nick = $_POST['nick'];
		if((strlen($nick)<3) || (strlen($nick)>20))
		{
			$ALL_OK = false;
			$_SESSION['e_nick']="Nick musi posiadac od 3 do 20 znaków!";
		}
			
		if(ctype_alnum($nick)==false)
		{
			$ALL_OK = false;
			$_SESSION['e_nick']="Nick może składać się tylko z liter i cyfr (bez polskich znaków)!";
		}
		
		//Spradzanie hasła
		$haslo1 = $_POST['haslo1'];
		$haslo2 = $_POST['haslo2'];
		if((strlen($haslo1) < 5) || (strlen($haslo1) > 20))
		{
			$ALL_OK = false;
			$_SESSION['e_haslo']="Haslo musi posiadac od 5 do 20 znaków!";
		}
		
		if($haslo1 != $haslo2)
		{
			$ALL_OK = false;
			$_SESSION['e_haslo']="Podane hasła nie są identyczne!";
		}
		
		//Recaptcha
		$sekret = "6LcFPU8UAAAAAGAGDhn-jCEGTiFQzYe8JpAmLDw1";
		
		$sprawdz = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$sekret.'&response='.$_POST['g-recaptcha-response']);
		
		$odpowiedz = json_decode($sprawdz);
		
		if(!($odpowiedz->success))
		{
			$ALL_OK = false;
			$_SESSION['e_bot']="Potwierdz ze nie jestes botem!";
		}
		
		$imie = $_POST['imie'];
		$nazwisko = $_POST['nazwisko'];
		
		//Zapamiętaj wprowadzone dane
		$_SESSION['save_nick'] = $nick;
		$_SESSION['save_imie'] = $imie;
		$_SESSION['save_nazwisko'] = $nazwisko;
		$_SESSION['save_haslo1'] = $haslo1;
		$_SESSION['save_haslo2'] = $haslo2;
		
		require_once "database.php"; //inicjalizujemy polacznie z baza danych

		//Sprawdzenie czy nick juz istanieje
		$userQuery = $db->prepare('SELECT id_gracza FROM gracze WHERE nick = :nick');
		$userQuery->bindValue(':nick', $nick, PDO::PARAM_STR);
		$userQuery->execute();

		if($userQuery->rowCount() > 0)
		{
			$ALL_OK = false;
			$_SESSION['e_nick']="Istnieje juz użytkownik o podanym nicku!";
		}		
		
		if($ALL_OK)
		{
			//Dodawanie nowego urzytkownika
			$query1 = $db->prepare('INSERT INTO gracze VALUES (NULL,:imie,:nazwisko,:nick,:haslo)');
			$query1->bindValue(':imie',$imie,PDO::PARAM_STR);
			$query1->bindValue(':nazwisko',$nazwisko,PDO::PARAM_STR);
			$query1->bindValue(':nick',$nick,PDO::PARAM_STR);
			$query1->bindValue(':haslo',$haslo1,PDO::PARAM_STR);
			$query1->execute();
			
			//Sprawdzanie id nowego uzytkownika
			$query2 = $db->prepare('SELECT id_gracza FROM gracze WHERE nick= :nick');
			$query2->bindValue(':nick', $nick, PDO::PARAM_STR);
			$query2->execute();
			$id_gracza = $query2->fetch();
			$kod_waluty = "PLN";
			$ilosc = 10000;
			
			//Dodawanie pieniedzy nowemu uzytkownikowi
			$query3= $db->prepare('INSERT INTO salda VALUES (:id_gracza,:kod_waluty,:ilosc)');
			$query3->bindValue(':id_gracza', $id_gracza['id_gracza'], PDO::PARAM_INT);
			$query3->bindValue(':kod_waluty', $kod_waluty, PDO::PARAM_STR);
			$query3->bindValue(':ilosc', $ilosc, PDO::PARAM_INT);
			$query3->execute();
			
			//Usuniecie zapoamietanych danych
			unset($_SESSION['save_nick']);
			unset($_SESSION['save_imie']);
			unset($_SESSION['save_nazwisko']);
			unset($_SESSION['save_haslo1']);
			unset($_SESSION['save_haslo2']);
			header('Location: witaj.php');
			exit();
		}
	}
	
?>

<!DOCTYPE HTML>

<html lang="pl">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Portfel inwestycyjny - </title>
	<script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>
	
	<h1>Portfel inwestycyjny - rejestracja</h1>
	<br\>
	Rejestacja:
	<form method="post">
	
		Nick name: </br> <input type="text" value="<?php
			if (isset($_SESSION['save_nick']))
			{
				echo $_SESSION['save_nick'];
				unset($_SESSION['save_nick']);
			}
		?>"  name="nick"/>
		<?php 
		if(isset($_SESSION['e_nick'])) 
		{
			echo $_SESSION['e_nick'];
			unset($_SESSION['e_nick']);
		}
		?>
		</br>
		
		
		Imię: <br/>  <input type="text" value="<?php
			if (isset($_SESSION['save_imie']))
			{
				echo $_SESSION['save_imie'];
				unset($_SESSION['save_imie']);
			}
		?>" name="imie"/> <br/> 

		
		Nazwisko: <br/>  <input type="text" value="<?php
			if (isset($_SESSION['save_nazwisko']))
			{
				echo $_SESSION['save_nazwisko'];
				unset($_SESSION['save_nazwisko']);
			}
		?>" name="nazwisko"/> <br/> 
		
		
		Hasło: <br/>  <input type="password" value="<?php
		if (isset($_SESSION['save_haslo1']))
		{
			echo $_SESSION['save_haslo1'];
			unset($_SESSION['save_haslo1']);
		}
		?>" name="haslo1"/> 
		<?php 
		if(isset($_SESSION['e_haslo'])) 
		{
			echo $_SESSION['e_haslo'];
			unset($_SESSION['e_haslo']);
		}
		?>
		</br>
		

		Powtór hasło: <br/>  <input type="password" value="<?php
		if (isset($_SESSION['save_haslo2']))
		{
			echo $_SESSION['save_haslo2'];
			unset($_SESSION['save_haslo2']);
		}
		?>" name="haslo2"/> <br/> 
	
	
		<div class="g-recaptcha" data-sitekey="6LcFPU8UAAAAALu7EHbzPfdpABbQ3XrQsNNsjKXw"></div>
		<?php 
		if(isset($_SESSION['e_bot'])) 
		{
			echo $_SESSION['e_bot'];
			unset($_SESSION['e_bot']);
		}
		?>
		</br> 
		
		</br>
		<input type="submit" value="Rejestacja">
	
	</form>

</body>
</html>